export class DayTwoGame {
  id: number;
  turns: DayTwoTurn[];

  constructor(game?: Partial<DayTwoGame>) {
    this.id = game?.id || 0;
    this.turns = game?.turns || [];
  }
}

export class DayTwoTurn {
  red: number;
  blue: number;
  green: number;

  constructor(turn?: Partial<DayTwoTurn>) {
    this.red = turn?.red || 0;
    this.blue = turn?.blue || 0;
    this.green = turn?.green || 0;
  }
}