import { Injectable } from '@nestjs/common';
import { Observable, filter, from, map, reduce } from 'rxjs';
import { DayTwoGame, DayTwoTurn } from './day-two.model';

@Injectable()
export class DayTwoService {
  private readonly RED_CUBES = 12;
  private readonly GREEN_CUBES = 13;
  private readonly BLUE_CUBES = 14;
  
  getPossibleGames(input: string): Observable<number> {
    return from(this.parseInput(input)).pipe(
      filter(game => game.turns.every(turn => turn.blue <= this.BLUE_CUBES && turn.green <= this.GREEN_CUBES && turn.red <= this.RED_CUBES)),
      reduce((acc, game) => acc + game.id, 0),
    );
  }

  getGamePowerSum(input: string): Observable<number> {
    return from(this.parseInput(input)).pipe(
      map(game => new DayTwoTurn({
        blue: Math.max(...game.turns.map(turn => turn.blue)),
        green: Math.max(...game.turns.map(turn => turn.green)),
        red: Math.max(...game.turns.map(turn => turn.red)),
      })),
      map(turn => turn.blue * turn.green * turn.red),
      reduce((acc, power) => acc + power, 0),
    );
  }

  private parseInput(input: string): Array<DayTwoGame> {
    return input
      .split('\n')
      .map(line => this.parseLine(line));
  }

  private parseLine(line: string): DayTwoGame {
    const [gameStr, turnsStr] = line.split(':');
    return new DayTwoGame({
      id: Number(gameStr.trim().split(' ')[1]),
      turns: turnsStr.split(';').map(turn => this.parseTurn(turn)),
    });
  }

  private parseTurn(turn: string): DayTwoTurn {
    const colors = turn.split(',').map(c => c.trim());
    const getColor = (color: string) => Number(colors.find(c => c.endsWith(color))?.split(' ')[0]);
    return new DayTwoTurn({
      blue: getColor('blue'),
      red: getColor('red'),
      green: getColor('green'),
    });
  }
}