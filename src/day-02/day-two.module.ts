import { Module } from '@nestjs/common';
import { DayTwoController } from './day-two.controller';
import { DayTwoService } from './day-two.service';

@Module({
  controllers: [DayTwoController],
  providers: [DayTwoService]
})
export class DayTwoModule {}
