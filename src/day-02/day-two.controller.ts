import { Body, Controller, Get } from '@nestjs/common';
import { DayTwoService } from './day-two.service';
import { Observable } from 'rxjs';

@Controller('day-two')
export class DayTwoController {
  constructor(private readonly service: DayTwoService) {}

  @Get('part-1')
  partOne(@Body('input') input: string): Observable<number> {
    return this.service.getPossibleGames(input);
  }

  @Get('part-2')
  partTwo(@Body('input') input: string): Observable<number> {
    return this.service.getGamePowerSum(input);
  }
}
