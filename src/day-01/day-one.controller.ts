import { Body, Controller, Get } from '@nestjs/common';
import { Observable } from 'rxjs';
import { DayOneService } from './day-one.service';

@Controller('day-one')
export class DayOneController {
  constructor(private readonly service: DayOneService) {}
  
  @Get('part-1')
  partOne(@Body('input') input: string): Observable<number> {
    return this.service.getFirstAndLastDigitSum(input);
  }

  @Get('part-2')
  partTwo(@Body('input') input: string): Observable<number> {
    return this.service.getSumWithLetters(input);
  }
}
