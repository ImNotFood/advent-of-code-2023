import { Injectable } from '@nestjs/common';
import { Observable, from, map, reduce, tap } from 'rxjs';

@Injectable()
export class DayOneService {
  getFirstAndLastDigitSum(input: string): Observable<number> {
    return from(this.parseInput(input)).pipe(
      map(line => `${line.match(/\d/)}${line.split('').reverse().join().match(/\d/)}`),
      map(numberStr => Number(numberStr)),
      reduce((acc, value) => acc + value, 0),
    );
  }

  getSumWithLetters(input: string): Observable<number> {
    return from(this.parseInput(input)).pipe(
      map(line => `${this.getFirstLetterDigit(line)}${this.getLastLetterDigit(line)}`),
      map(numberStr => Number(numberStr)),
      reduce((acc, value) => acc + value, 0),
    );
  }

  getFirstLetterDigit(line: string): string {
    if (!isNaN(Number(line[0]))) {
      return line[0];
    }

    const spelledOutDigit = digits.find((digit) => line.startsWith(digit));
    if (spelledOutDigit) {
      return digits.indexOf(spelledOutDigit).toString();
    }
    return this.getFirstLetterDigit(line.slice(1));
  }

  getLastLetterDigit(line: string): string {
    if (!isNaN(Number(line.at(-1)))) {
      return line.at(-1)!;
    }

    const spelledOutDigit = digits.find(digit => line.endsWith(digit));
    if (spelledOutDigit) {
      return digits.indexOf(spelledOutDigit).toString();
    }
    return this.getLastLetterDigit(line.slice(0, -1));
  }

  private parseInput(input: string): string[] {
    return input.split('\n').map(str => str.trim());
  }
}

const digits = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];