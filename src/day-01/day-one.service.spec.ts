import { Test, TestingModule } from '@nestjs/testing';
import { DayOneService } from './day-one.service';

describe('DayOneService', () => {
  let service: DayOneService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DayOneService],
    }).compile();

    service = module.get<DayOneService>(DayOneService);
  });

  const dayOneInput = `1abc2
  pqr3stu8vwx
  a1b2c3d4e5f
  treb7uchet`;

  it('should return sum for day one', done => {
    service.getFirstAndLastDigitSum(dayOneInput).subscribe(result => {
      expect(result).toBe(142);
      done();
    })
  });

  const dayTwoInput = `two1nine
  eightwothree
  abcone2threexyz
  xtwone3four
  4nineeightseven2
  zoneight234
  7pqrstsixteen`;

  it('should return sum for day two', done => {
    service.getSumWithLetters(dayTwoInput).subscribe(result => {
      expect(result).toBe(281);
      done();
    });
  });
});
