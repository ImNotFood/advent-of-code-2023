import { Module } from '@nestjs/common';
import { DayOneModule } from './day-01/day-one.module';
import { DayTwoModule } from './day-02/day-two.module';

@Module({
  imports: [DayOneModule, DayTwoModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
